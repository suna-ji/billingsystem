package application.controller;

import application.entity.Customer;
import application.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.Collection;

@RestController
public class MainController {

    @Autowired
    CustomerService customerService;

    @GetMapping("/user/me")
    public Principal user(Principal principal) {
        return principal;
    }

    @GetMapping("/get/customers")
    public Collection<Customer> getAllCustomers() {
        return customerService.getAllCustomers();
    }
}