package application.service;

import application.constants.RatingEngineDTO;
import application.entity.*;
import application.enums.ServiceType;
import application.model.Notification;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static application.constants.RatingEngineUtils.*;

@Data
@Service
public class RatingServiceImpl implements RatingService {

    @Autowired
    private BillingService billingService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private TariffService tariffService;

    private RatingEngineDTO ratingEngineDTO;

    @Override
    public Notification calculateAndGetNotificationToSendUser(CallDetailRecord cdr) {
        Notification notification = null;
        if (cdr instanceof TrafficCallDetailRecord) {
            notification = calculateTraffic((TrafficCallDetailRecord) cdr);
        }
        if (cdr instanceof VoiceCallDetailRecord) {
            notification = calculateVoice((VoiceCallDetailRecord) cdr);
        }
        if (cdr instanceof SMSCallDetailRecord) {
            notification = calculateSMS((SMSCallDetailRecord) cdr);
        }
        return notification;
    }

    private Notification calculateTraffic(TrafficCallDetailRecord traffic) {
        initializeFieldsForCalculatingTraffic(traffic);
        if ((ratingEngineDTO.getTariff().getMonthlyPrice() != 0) && (ratingEngineDTO.getRemainingTraffic() > 0)) {
            if (ratingEngineDTO.getTrafficInMB() >= ratingEngineDTO.getRemainingTraffic()) {
                return calculateMixedTraffic();
            } else {
                return calculateFreeTraffic();
            }
        } else {
            return calculateNotFreeTraffic();
        }
    }

    private Notification calculateVoice(VoiceCallDetailRecord voice) {
        initializeFieldsForCalculatingVoice(voice);
        if ((ratingEngineDTO.getTariff().getMonthlyPrice() != 0) && (ratingEngineDTO.getRemainingSeconds() > 0)) {
            if (ratingEngineDTO.getCallDuration() > ratingEngineDTO.getRemainingSeconds()) {
                return calculateMixedVoice();
            } else {
                return calculateFreeVoice();
            }
        } else {
            return calculateNotFreeVoice();
        }
    }

    private Notification calculateSMS(SMSCallDetailRecord sms) {
        initializeFieldsForCalculatingSMS(sms);
        if ((ratingEngineDTO.getTariff().getMonthlyPrice() != 0) && (ratingEngineDTO.getRemainingSMS() > 0)) {
            if (ratingEngineDTO.getNumberOfSMS() > ratingEngineDTO.getRemainingSMS()) {
                return calculateMixedSMS();
            } else {
                return calculateFreeSMS();
            }
        } else {
            return calculateNotFreeSMS();
        }
    }

    private void initializeFieldsForCalculatingTraffic(TrafficCallDetailRecord traffic) {
        ratingEngineDTO = new RatingEngineDTO(customerService.getCustomerByServiceId(traffic.getSourceServiceId()),
                traffic.getStartDate(),
                traffic.getTrafficVolume() / (1024. * 1024),
                null,
                null,
                null,
                null,
                traffic.getSourceServiceId());
    }

    private void initializeFieldsForCalculatingVoice(VoiceCallDetailRecord voice) {
        ratingEngineDTO = new RatingEngineDTO(customerService.getCustomerByServiceId(voice.getSourceServiceId()),
                voice.getEndDate(),
                null,
                voice.getCallDuration(),
                null,
                null,
                null,
                voice.getSourceServiceId());
    }

    private void initializeFieldsForCalculatingSMS(SMSCallDetailRecord sms) {
        ratingEngineDTO = new RatingEngineDTO(customerService.getCustomerByServiceId(sms.getSourceServiceId()),
                sms.getStartDate(),
                null,
                null,
                sms.getSmsSize() / MAX_SMS_SIZE + 1,
                null,
                null,
                sms.getSourceServiceId());
    }

    private Notification calculateNotFreeTraffic() {
        ratingEngineDTO.setSubtotal(ratingEngineDTO.getTariff().getPricePerMegabyte() * ratingEngineDTO.getTrafficInMB());
        ratingEngineDTO.setTotal((1 - ratingEngineDTO.getDiscount()) * ratingEngineDTO.getSubtotal());
        ratingEngineDTO.getCustomer().setBalance(ratingEngineDTO.getCustomer().getBalance() - ratingEngineDTO.getTotal());
        customerService.updateCustomer(ratingEngineDTO.getCustomer());
        billingService.save(new Bill(DESCRIPTION, ratingEngineDTO.getCreationDate(), CURRENCY_CODE, STATUS_BILLED, ratingEngineDTO.getSubtotal(), ratingEngineDTO.getDiscount(), ratingEngineDTO.getTotal(), ServiceType.TRAFFIC, ratingEngineDTO.getCustomer()));
        return new Notification(ratingEngineDTO.getSourceServiceId(), ratingEngineDTO.getTotal(), ServiceType.TRAFFIC, 0L);
    }

    private Notification calculateFreeTraffic() {
        Double remainingMegabytes = ratingEngineDTO.getRemainingTraffic() - ratingEngineDTO.getTrafficInMB();
        ratingEngineDTO.getCustomer().setRemainingMegabytes(remainingMegabytes);
        customerService.updateCustomer(ratingEngineDTO.getCustomer());
        billingService.save(new Bill(DESCRIPTION, ratingEngineDTO.getCreationDate(), CURRENCY_CODE, STATUS_BILLED, 0., 0., 0., ServiceType.TRAFFIC, ratingEngineDTO.getCustomer()));
        return new Notification(ratingEngineDTO.getSourceServiceId(), 0.,
                ServiceType.TRAFFIC, remainingMegabytes.longValue());
    }

    private Notification calculateMixedTraffic() {
        ratingEngineDTO.getCustomer().setRemainingMegabytes(0.);
        ratingEngineDTO.setSubtotal((ratingEngineDTO.getTrafficInMB() - ratingEngineDTO.getRemainingTraffic()) * ratingEngineDTO.getTariff().getPricePerMegabyte());
        ratingEngineDTO.setTotal((1 - ratingEngineDTO.getDiscount()) * ratingEngineDTO.getSubtotal());
        ratingEngineDTO.getCustomer().setBalance(ratingEngineDTO.getCustomer().getBalance() - ratingEngineDTO.getTotal());
        customerService.updateCustomer(ratingEngineDTO.getCustomer());
        billingService.save(new Bill(DESCRIPTION, ratingEngineDTO.getCreationDate(), CURRENCY_CODE, STATUS_BILLED, ratingEngineDTO.getSubtotal(), ratingEngineDTO.getDiscount(), ratingEngineDTO.getTotal(), ServiceType.TRAFFIC, ratingEngineDTO.getCustomer()));
        return new Notification(ratingEngineDTO.getSourceServiceId(), ratingEngineDTO.getTotal(), ServiceType.TRAFFIC, 0L);
    }

    private Notification calculateNotFreeVoice() {
        ratingEngineDTO.setSubtotal((ratingEngineDTO.getCallDuration() / 60 + 1) * ratingEngineDTO.getTariff().getPricePerMinute());
        ratingEngineDTO.setTotal((1 - ratingEngineDTO.getDiscount()) * ratingEngineDTO.getSubtotal());
        ratingEngineDTO.getCustomer().setBalance(ratingEngineDTO.getCustomer().getBalance() - ratingEngineDTO.getTotal());
        customerService.updateCustomer(ratingEngineDTO.getCustomer());
        billingService.save(new Bill(DESCRIPTION, ratingEngineDTO.getCreationDate(), CURRENCY_CODE, STATUS_BILLED, ratingEngineDTO.getSubtotal(), ratingEngineDTO.getDiscount(), ratingEngineDTO.getTotal(), ServiceType.VOICE, ratingEngineDTO.getCustomer()));
        return new Notification(ratingEngineDTO.getSourceServiceId(), ratingEngineDTO.getTotal(), ServiceType.VOICE, 0L);
    }

    private Notification calculateFreeVoice() {
        ratingEngineDTO.getCustomer().setRemainingSeconds(ratingEngineDTO.getRemainingSeconds() - ratingEngineDTO.getCallDuration());
        customerService.updateCustomer(ratingEngineDTO.getCustomer());
        billingService.save(new Bill(DESCRIPTION, ratingEngineDTO.getCreationDate(), CURRENCY_CODE, STATUS_BILLED, 0., 0., 0., ServiceType.VOICE, ratingEngineDTO.getCustomer()));
        return new Notification(ratingEngineDTO.getSourceServiceId(), 0.,
                ServiceType.VOICE, ratingEngineDTO.getRemainingSeconds() - ratingEngineDTO.getCallDuration());
    }

    private Notification calculateMixedVoice() {
        ratingEngineDTO.getCustomer().setRemainingSeconds(0L);
        ratingEngineDTO.setSubtotal(((ratingEngineDTO.getCallDuration() - ratingEngineDTO.getRemainingSeconds()) / 60 + 1) * ratingEngineDTO.getTariff().getPricePerMinute());
        ratingEngineDTO.setTotal((1 - ratingEngineDTO.getDiscount()) * ratingEngineDTO.getSubtotal());
        ratingEngineDTO.getCustomer().setBalance(ratingEngineDTO.getCustomer().getBalance() - ratingEngineDTO.getTotal());
        customerService.updateCustomer(ratingEngineDTO.getCustomer());
        billingService.save(new Bill(DESCRIPTION, ratingEngineDTO.getCreationDate(), CURRENCY_CODE, STATUS_BILLED, ratingEngineDTO.getSubtotal(), ratingEngineDTO.getDiscount(), ratingEngineDTO.getTotal(), ServiceType.VOICE, ratingEngineDTO.getCustomer()));
        return new Notification(ratingEngineDTO.getSourceServiceId(), ratingEngineDTO.getTotal(), ServiceType.VOICE, 0L);
    }

    private Notification calculateNotFreeSMS() {
        ratingEngineDTO.setSubtotal(ratingEngineDTO.getTariff().getPricePerSMS() * ratingEngineDTO.getNumberOfSMS());
        ratingEngineDTO.setTotal((1 - ratingEngineDTO.getDiscount()) * ratingEngineDTO.getSubtotal());
        ratingEngineDTO.getCustomer().setBalance(ratingEngineDTO.getCustomer().getBalance() - ratingEngineDTO.getTotal());
        customerService.updateCustomer(ratingEngineDTO.getCustomer());
        billingService.save(new Bill(DESCRIPTION, ratingEngineDTO.getCreationDate(), CURRENCY_CODE, STATUS_BILLED, ratingEngineDTO.getSubtotal(), ratingEngineDTO.getDiscount(), ratingEngineDTO.getTotal(), ServiceType.SMS, ratingEngineDTO.getCustomer()));
        return new Notification(ratingEngineDTO.getSourceServiceId(), ratingEngineDTO.getTotal(), ServiceType.SMS, 0L);
    }

    private Notification calculateFreeSMS() {
        ratingEngineDTO.getCustomer().setRemainingSMS(ratingEngineDTO.getRemainingSMS() - ratingEngineDTO.getNumberOfSMS());
        customerService.updateCustomer(ratingEngineDTO.getCustomer());
        billingService.save(new Bill(DESCRIPTION, ratingEngineDTO.getCreationDate(), CURRENCY_CODE, STATUS_BILLED, 0., 0., 0., ServiceType.SMS, ratingEngineDTO.getCustomer()));
        return new Notification(ratingEngineDTO.getSourceServiceId(), 0.,
                ServiceType.SMS, ratingEngineDTO.getRemainingSMS() - ratingEngineDTO.getNumberOfSMS());
    }

    private Notification calculateMixedSMS() {
        ratingEngineDTO.getCustomer().setRemainingSMS(0L);
        ratingEngineDTO.setSubtotal((ratingEngineDTO.getNumberOfSMS() - ratingEngineDTO.getRemainingSMS()) * ratingEngineDTO.getTariff().getPricePerSMS());
        ratingEngineDTO.setTotal((1 - ratingEngineDTO.getDiscount()) * ratingEngineDTO.getSubtotal());
        ratingEngineDTO.getCustomer().setBalance(ratingEngineDTO.getCustomer().getBalance() - ratingEngineDTO.getTotal());
        customerService.updateCustomer(ratingEngineDTO.getCustomer());
        billingService.save(new Bill(DESCRIPTION, ratingEngineDTO.getCreationDate(), CURRENCY_CODE, STATUS_BILLED, ratingEngineDTO.getSubtotal(), ratingEngineDTO.getDiscount(), ratingEngineDTO.getTotal(), ServiceType.SMS, ratingEngineDTO.getCustomer()));
        return new Notification(ratingEngineDTO.getSourceServiceId(), ratingEngineDTO.getTotal(), ServiceType.SMS, 0L);
    }
}

