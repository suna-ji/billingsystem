package application.controller;

import application.entity.CallDetailRecord;
import application.model.CallDetailRecordModel;
import application.model.Notification;
import application.service.CallDetailRecordService;
import application.service.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
public class CallDetailRecordController {

    @Autowired
    private CallDetailRecordService callDetailRecordService;

    @Autowired
    private RatingService ratingService;

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    @GetMapping("/get/cdrs")
    public Collection<CallDetailRecord> getAllCallDetailsRecords() {
        return callDetailRecordService.getAllCallDetailRecords();
    }

    @PostMapping("/add/cdr")
    public CallDetailRecord addCallDetailRecord(
            @RequestBody CallDetailRecordModel callDetailRecordModel) {
        CallDetailRecord cdr = callDetailRecordService.createCallDetailRecord(callDetailRecordModel);
        callDetailRecordService.save(cdr);
        Notification notification = ratingService.calculateAndGetNotificationToSendUser(cdr);
        messagingTemplate.convertAndSendToUser(notification.getServiceId().toString(),
                "/queue/reply", notification);
        return cdr;
    }

}
