package application.entity.embeddable;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Embeddable
public class Roaming {

    @Column(name = "roaming_code")
    @NonNull
    private Integer roamingCode;

}
