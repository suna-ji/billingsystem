package application.entity;

import application.enums.Disposition;
import application.enums.Status;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@Entity
@Table(name = "cdrs")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "service_type")
public abstract class CallDetailRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "global_id")
    private Long globalId;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", length = 16)
    private Status status;

    @Column(name = "source_country_code")
    private Integer sourceCountryCode;

    @Column(name = "source_service_id")
    private Long sourceServiceId;

    @Column(name = "start_date")
    private Date startDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "disposition", length = 16)
    private Disposition disposition;

    public CallDetailRecord(Status status, Integer sourceCountryCode, Long sourceServiceId, Date startDate, Disposition disposition) {
        this.status = status;
        this.sourceCountryCode = sourceCountryCode;
        this.sourceServiceId = sourceServiceId;
        this.startDate = startDate;
        this.disposition = disposition;
    }
}
