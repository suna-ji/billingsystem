package application.database;

import application.entity.Customer;
import application.entity.Payment;
import application.hibernate.HibernateUtil;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

@Repository
public class PaymentDAOImpl implements PaymentDAO {

    @Override
    public void addPayment(Payment payment) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(payment);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public Payment getPaymentById(Long payment_id) {
        Session session = null;
        Payment payment = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            payment = session.load(Payment.class, payment_id);
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return payment;
    }

    @Override
    public List<Payment> getPaymentsByCustomer(Customer customer) {
        return customer.getPayments();
    }

    @Override
    public Collection<Payment> getAllPayments() {
        Session session = null;
        List<Payment> payments = new LinkedList<>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            payments = session.createCriteria(Payment.class).list();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return payments;
    }

    @Override
    public void deletePayment(Payment payment) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(payment);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }
}




