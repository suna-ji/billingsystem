package application.database;

import application.entity.Customer;

import java.sql.SQLException;
import java.util.Collection;

public interface CustomerDAO {
    void addCustomer(Customer customer) throws SQLException;

    void updateCustomer(Customer customer) throws SQLException;

    Customer getCustomerByServiceId(Long serviceId) throws SQLException;

    Collection<Customer> getAllCustomers() throws SQLException;

    void deleteCustomer(Customer customer) throws SQLException;
}