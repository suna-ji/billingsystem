package application.database;

import application.entity.Region;

import java.sql.SQLException;
import java.util.Collection;

public interface RegionDAO {
    void addRegion(Region region) throws SQLException;

    Region getRegionById(Long regionId) throws SQLException;

    Collection<Region> getAllRegions() throws SQLException;
}
