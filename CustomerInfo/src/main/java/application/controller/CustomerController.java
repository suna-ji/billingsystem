package application.controller;

import application.entity.Customer;
import application.entity.embeddable.CustomerLimits;
import application.entity.embeddable.CustomerName;
import application.model.CustomerModel;
import application.service.CustomerServiceImpl;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerController {
    @Autowired
    CustomerServiceImpl customerService;

    @PostMapping(value = "/customer/balance/update")
    public String updateBalance(Authentication authentication,
                                @RequestParam(name = "payment", required = true) Double paymentValue) {
        Customer customer = customerService.getCustomerByServiceId(Long.parseLong(authentication.getName()));
        return (new JSONObject())
                .put("result", customerService.updateBalance(customer, paymentValue)).toString();
    }

    @PostMapping(value = "/customer/update")
    public String updateCustomer(Authentication authentication,
                                 @RequestParam(name = "email", required = false) String email,
                                 @RequestParam(name = "additionalServiceId", required = false) Long additionalServiceId) {
        Customer customer = customerService.getCustomerByServiceId(Long.parseLong(authentication.getName()));
        return (new JSONObject())
                .put("result", customerService.updateCustomer(customer, email, additionalServiceId)).toString();
    }

    @PostMapping(value = "/customer/update/tariff")
    public String updateCustomerTariff(Authentication authentication,
                                 @RequestParam(name = "tariffName", required = true) String tariffName) {
        Customer customer = customerService.getCustomerByServiceId(Long.parseLong(authentication.getName()));
        return (new JSONObject())
                .put("result", customerService.updateCustomerTariff(customer, tariffName)).toString();
    }

    @GetMapping(value = "/customer/info")
    public CustomerModel getCustomerModel(Authentication authentication) {
        return customerService.createCustomerModel(Long.valueOf(authentication.getName()));
    }

    @GetMapping(value = "/customer/tariffName")
    public String getCustomerTariffName(Authentication authentication) {
        Customer customer = customerService.getCustomerByServiceId(Long.valueOf(authentication.getName()));
        return (new JSONObject()).put("tariffName", customer.getTariff().getName()).toString();
    }

    @GetMapping(value = "/customer/name")
    public CustomerName getCustomerName(Authentication authentication) {
        return customerService.getCustomerNameByServiceId(Long.valueOf(authentication.getName()));
    }

    @GetMapping(value = "/customer/balance")
    public Double getBalance(Authentication authentication) {
        return customerService.getBalance(Long.valueOf(authentication.getName()));
    }

    @GetMapping(value = "/customer/limits")
    public CustomerLimits getLimits(Authentication authentication) {
        return customerService.getLimits(Long.valueOf(authentication.getName()));
    }

    @GetMapping(value = "/customer/serviceId")
    public Long getServiceId(Authentication authentication) {
        return customerService.getCustomerByServiceId(Long.parseLong(authentication.getName())).getServiceId();
    }

}
