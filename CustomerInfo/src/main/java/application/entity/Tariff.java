package application.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@ToString(exclude = {"customers"})
@EqualsAndHashCode(of = {"id"})
@Entity
@Table(name = "tariffs")
@Proxy(lazy = false)
public class Tariff {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tariff_id")
    private Long id;

    /**
     * The name of the tariff
     */
    @Column(name = "name")
    private String name;

    /**
     * The price which customer should pay each month to get free seconds / SMS / MBs and to be able to use services
     */
    @Column(name = "monthly_price")
    private Double monthlyPrice;

    /**
     * Number of seconds that customer has for free
     */
    @Column(name = "free_seconds")
    private Long freeSeconds;

    /**
     * Number of SMS that customer has for free
     */
    @Column(name = "free_sms")
    private Long freeSMS;

    /**
     * Number of MBs that customer has for free
     */
    @Column(name = "free_megabytes")
    private Double freeMegabytes;

    /**
     * The price for one minute of talk
     */
    @Column(name = "price_minute")
    private Double pricePerMinute;

    /**
     * The price for one SMS
     */
    @Column(name = "price_sms")
    private Double pricePerSMS;

    /**
     * The price for one MB of traffic
     */
    @Column(name = "price_megabyte")
    private Double pricePerMegabyte;

    /**
     * The value of discount (from 0 to 1) that is used to decrease price for all customers for all services
     */
    @Column(name = "discount")
    private Double discount;

    /**
     * Customers that have the tariff
     */
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "tariff")
    private List<Customer> customers;

    /**
     * Shows if the tariff is available for transition
     */
    @Column(name = "available")
    private boolean isAvailable;

    public Tariff(String name, Double monthlyPrice, Long freeSeconds, Long freeSMS, Double freeMegabytes, Double pricePerMinute, Double pricePerSMS, Double pricePerMegabyte, Double discount) {
        this(name, monthlyPrice, freeSeconds, freeSMS, freeMegabytes, pricePerMinute, pricePerSMS, pricePerMegabyte, discount, new ArrayList<>(), true);
    }

    public Tariff(String name, Double monthlyPrice, Long freeSeconds, Long freeSMS, Double freeMegabytes, Double pricePerMinute, Double pricePerSMS, Double pricePerMegabyte, Double discount, List<Customer> customers, boolean isAvailable) {
        this.name = name;
        this.monthlyPrice = monthlyPrice;
        this.freeSeconds = freeSeconds;
        this.freeSMS = freeSMS;
        this.freeMegabytes = freeMegabytes;
        this.pricePerMinute = pricePerMinute;
        this.pricePerSMS = pricePerSMS;
        this.pricePerMegabyte = pricePerMegabyte;
        this.discount = discount;
        this.customers = customers;
        this.isAvailable = isAvailable;
    }

}
