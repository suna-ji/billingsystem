package application.entity;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.util.Date;

@Data
@ToString(exclude = {"customer"})
@EqualsAndHashCode(exclude = {"customer"})
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(name = "payments")
@Proxy(lazy = false)
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @NonNull
    @JoinColumn(name = "service_id")
    private Customer customer;

    @CreationTimestamp
    @Column(name = "payment_date")
    private Date paymentDate;

    @Column(name = "payment")
    @NonNull
    private Double payment;

    @Column(name = "balance_after_payment")
    @NonNull
    private Double balanceAfterPayment;

}


