package application.service;

import application.database.CustomerInfoFactory;
import application.entity.Tariff;
import application.model.TariffModel;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class TariffServiceImpl implements TariffService {


    @Override
    public List<TariffModel> getAllTariffModels() {
        List<TariffModel> tariffs = new ArrayList<>();
        CustomerInfoFactory.getInstance().getTariffDAO().getAllTariffs()
                .forEach(tariff -> tariffs.add(new TariffModel(tariff)));
        return tariffs;
    }

    @Override
    public List<TariffModel> getAvailableTariffModels() {
        List<TariffModel> tariffs = new ArrayList<>();
        CustomerInfoFactory.getInstance().getTariffDAO().getAvailableTariffs()
                .forEach(tariff -> tariffs.add(new TariffModel(tariff)));
        return tariffs;
    }

    @Override
    public TariffModel getTariffModelById(Long tariffId) {
        try {
            return new TariffModel(getTariffById(tariffId));
        } catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public Tariff getTariffById(Long tariffId) {
        try {
            return CustomerInfoFactory.getInstance().getTariffDAO().getTariffById(tariffId);
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public Tariff getTariffByName(String tariffName) {
        try {
            return CustomerInfoFactory.getInstance().getTariffDAO().getTariffByName(tariffName);
        } catch (SQLException e) {
            return null;
        }
    }


    @Override
    public void save(Tariff tariff) {
        try {
            CustomerInfoFactory.getInstance().getTariffDAO().addTariff(tariff);
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
