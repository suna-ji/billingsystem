package application.service;

import application.entity.Tariff;
import application.model.TariffModel;

import java.util.List;

public interface TariffService {
    List<TariffModel> getAllTariffModels();

    List<TariffModel> getAvailableTariffModels();

    TariffModel getTariffModelById(Long tariffId);

    Tariff getTariffById(Long tariffId);

    Tariff getTariffByName(String tariffName);

    void save(Tariff tariff);
}
