package application.service;

import application.database.CustomerInfoFactory;
import application.entity.Customer;
import application.entity.Tariff;
import application.entity.embeddable.CustomerLimits;
import application.entity.embeddable.CustomerName;
import application.model.CustomerModel;
import org.springframework.beans.factory.annotation.Autowired;
import application.entity.Payment;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService, UserDetailsService {

    @Autowired
    private PaymentServiceImpl paymentService;
    @Autowired
    RegionService regionService;
    @Autowired
    TariffService tariffService;

    @Override
    public Collection<Customer> getAllCustomers() {
        try {
            return CustomerInfoFactory.getInstance().getCustomerDAO().getAllCustomers();
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public CustomerModel createCustomerModel(Long serviceId) {
        Customer customer = getCustomerByServiceId(serviceId);
        return new CustomerModel(customer.getServiceId(), customer.getPersonalAccount(),
                customer.getFirstName(), customer.getMiddleName(), customer.getPatronymic(), customer.getLastName(),
                customer.getRegion().getRegionName() + ", " + customer.getRegion().getCountry().getCountryName(),
                customer.getTariff().getId(), customer.getLimits(),
                customer.getCreationDate(), customer.getBirthday(),
                customer.getEmail(), customer.getAdditionalServiceId());
    }

    @Override
    public void updateCustomer(Customer customer) {
        try {
            CustomerInfoFactory.getInstance().getCustomerDAO().updateCustomer(customer);
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public Customer getCustomerByServiceId(Long serviceId) {
        try {
            return CustomerInfoFactory.getInstance().getCustomerDAO().getCustomerByServiceId(serviceId);
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public CustomerName getCustomerNameByServiceId(Long serviceId) {
        try {
            return getCustomerByServiceId(serviceId).getName();
        } catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public void save(Customer customer) {
        try {
            CustomerInfoFactory.getInstance().getCustomerDAO().addCustomer(customer);
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private List getAuthority() {
        return Arrays.asList(new SimpleGrantedAuthority("ADMIN"));
    }

    public UserDetails loadUserByUsername(String serviceIdString) throws UsernameNotFoundException {

        Long serviceId;
        try {
            serviceId = Long.valueOf(serviceIdString);
        } catch (NumberFormatException e) {
            throw new UsernameNotFoundException("Invalid service id or password.");
        }

        Customer customer = null;
        try {
            customer = CustomerInfoFactory.getInstance().getCustomerDAO().getCustomerByServiceId(serviceId);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (customer == null) {
            throw new UsernameNotFoundException("Invalid service id or password.");
        }
        return new org.springframework.security.core.userdetails.User(String.valueOf(customer.getServiceId()), customer.getPassword(), getAuthority());
    }

    @Override
    public Double getBalance(Long serviceId) {
        return getCustomerByServiceId(serviceId).getBalance();
    }

    @Override
    public CustomerLimits getLimits(Long serviceId) {
        return getCustomerByServiceId(serviceId).getLimits();
    }

    @Override
    public String updateBalance(Customer customer, Double paymentValue) {
        if (paymentValue <= 0)
            return "failed";
        double balanceAfterPayment = Math.round((customer.getBalance() + paymentValue) * 100) / 100;
        paymentService.save(new Payment(customer, paymentValue,
                balanceAfterPayment));
        customer.setBalance(customer.getBalance() + paymentValue);
        updateCustomer(customer);
        return "success";
    }

    @Override
    public String updateCustomerTariff(Customer customer, String newTariffName) {
        Tariff tariff = tariffService.getTariffByName(newTariffName);
        if (tariff == null || customer.getBalance() < tariff.getMonthlyPrice()) {
            return "failed";
        }
        customer.setBalance(customer.getBalance() - tariff.getMonthlyPrice());
        customer.setTariff(tariff);
        customer.setBalance(customer.getBalance() - tariff.getMonthlyPrice());
        customer.setRemainingSeconds(tariff.getFreeSeconds());
        customer.setRemainingSMS(tariff.getFreeSMS());
        customer.setRemainingMegabytes(tariff.getFreeMegabytes());
        updateCustomer(customer);
        return "success";
    }

    @Override
    public String updateCustomer(Customer customer, String email, Long additionalServiceId) {
        customer.setEmail(email);
        customer.setAdditionalServiceId(additionalServiceId);
        updateCustomer(customer);
        return "success";
    }

}
