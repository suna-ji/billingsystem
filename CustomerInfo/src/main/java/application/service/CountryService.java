package application.service;

import application.entity.Country;

import java.util.Collection;

public interface CountryService {
    Collection<Country> getAllCountries();

    Country getCountryById(Long countryId);

    Country getCountryByCountryCode(Integer countryCode);

    void save(Country country);
}
