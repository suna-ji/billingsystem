package application.enums;

public enum Status {
    RECEIVED, DUPLICATED, ERROR, RATED, BILLED
}
