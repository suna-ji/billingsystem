package application.enums;

public enum TrafficQuality {
    PRIORITY, SPEED, LEASEDLINE
}
