package application.enums;

public enum TrafficUnits {
    B, KB, MB, GB
}
