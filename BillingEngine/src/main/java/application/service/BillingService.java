package application.service;

import application.entity.Bill;

import java.util.Collection;

public interface BillingService {

    Collection<Bill> getAllBills();

    Collection<Bill> getBillsByCustomerId(Long serviceId);

    void save(Bill bill);

    void update(Bill bill);
}