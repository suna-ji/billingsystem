package application.util;

import application.entity.Bill;
import application.entity.Customer;
import application.enums.ServiceType;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class GeneratePdfReport {

    private static final String HEADER = "BillingEngine/src/main/resources/invoice_header.png";
    private static final String DIVIDER = "BillingEngine/src/main/resources/divider.png";

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    private static double getPeriodSubtotal(List<Bill> bills, String serviceType) {
        double sum = 0;
        for (Bill bill : bills) {
            if (bill.getServiceType().equals(ServiceType.valueOf(serviceType.toUpperCase())))
                sum += bill.getSubtotal();
        }
        return Math.round(sum);
    }

    private static double getPeriodDiscount(List<Bill> bills, String serviceType) {
        double sum = 0;
        for (Bill bill : bills) {
            if (bill.getServiceType().equals(ServiceType.valueOf(serviceType.toUpperCase())))
                sum += bill.getDiscount();
        }
        return Math.round(sum);
    }

    private static double getPeriodTotal(List<Bill> bills, String serviceType) {
        return getPeriodSubtotal(bills, serviceType) - getPeriodDiscount(bills, serviceType);
    }

    private static void createCell(PdfPTable table, String columnName, Font font) {
        PdfPCell cell = new PdfPCell(new Phrase(columnName, font));
        table.addCell(cell);
    }


    private static void createCell(PdfPTable table, String columnName, Font font, int hAlignment) {
        PdfPCell cell = new PdfPCell(new Phrase(columnName, font));
        cell.setHorizontalAlignment(hAlignment);
        table.addCell(cell);
    }

    private static void createCell(PdfPTable table, String columnName, Font font, int hAlignment, int vAlignment) {
        PdfPCell cell = new PdfPCell(new Phrase(columnName, font));
        cell.setHorizontalAlignment(hAlignment);
        cell.setVerticalAlignment(vAlignment);
        table.addCell(cell);
    }

    private static void createCell(PdfPTable table, String columnName, Font font, int hAlignment, int vAlignment, int border) {
        PdfPCell cell = new PdfPCell(new Phrase(columnName, font));
        cell.setHorizontalAlignment(hAlignment);
        cell.setVerticalAlignment(vAlignment);
        cell.setBorder(border);
        table.addCell(cell);
    }

    private static void createEmptyCell(PdfPTable table) {
        PdfPCell cell = new PdfPCell(new Phrase(""));
        cell.setBorder(0);
        table.addCell(cell);
    }

    private static ByteArrayInputStream createHeader(PdfPTable table, Customer customer, String headerName, String dateFrom, String dateTo) throws IOException, DocumentException {
        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Font font = new Font(Font.FontFamily.HELVETICA);

        Image image = Image.getInstance(HEADER);
        PdfPTable imgTable = new PdfPTable(2);
        imgTable.setWidthPercentage(75);

        PdfPCell cell = new PdfPCell(image);
        cell.setBorder(0);
        imgTable.addCell(cell);

        createCell(imgTable, headerName, FontFactory.getFont(FontFactory.HELVETICA_BOLD, 20, Font.BOLD),
                Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 0);

        PdfWriter.getInstance(document, out);
        document.open();

        image = Image.getInstance(DIVIDER);
        image.setAlignment(Element.ALIGN_CENTER);
        document.add(image);
        document.add(imgTable);

        Paragraph paragraph = new Paragraph("City: " + customer.getRegion().getCountry().getCountryName()
                + ", " + customer.getRegion().getRegionName(), font);
        paragraph.setAlignment(Element.ALIGN_LEFT);
        paragraph.setIndentationLeft(50);
        document.add(paragraph);

        DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
        Date date = new Date();
        paragraph = new Paragraph("Date: " + dateFormat.format(date), font);
        paragraph.setAlignment(Element.ALIGN_LEFT);
        paragraph.setIndentationLeft(50);
        document.add(paragraph);

        image = Image.getInstance(DIVIDER);
        image.setAlignment(Element.ALIGN_CENTER);
        document.add(image);

        String fullName = customer.getFirstName() + " " + customer.getLastName();
        if (customer.getMiddleName() != null) {
            fullName = customer.getFirstName() + " " + customer.getMiddleName() + " " + customer.getLastName();
        }

        paragraph = new Paragraph("NAME: " + fullName, font);
        paragraph.setAlignment(Element.ALIGN_LEFT);
        paragraph.setIndentationLeft(50);
        document.add(paragraph);

        paragraph = new Paragraph("NUMBER: " + "+" + customer.getServiceId(), font);
        paragraph.setAlignment(Element.ALIGN_LEFT);
        paragraph.setIndentationLeft(50);
        document.add(paragraph);

        paragraph = new Paragraph("PERIOD: " + dateFormat.format(new Date(Long.parseLong(dateFrom)))
                + " - " + dateFormat.format(new Date(Long.parseLong(dateTo))), font);
        paragraph.setAlignment(Element.ALIGN_LEFT);
        paragraph.setIndentationLeft(50);
        document.add(paragraph);

        document.add(Chunk.NEWLINE);
        document.add(table);

        image = Image.getInstance(DIVIDER);
        image.setAlignment(Element.ALIGN_CENTER);
        document.add(image);

        document.close();

        return new ByteArrayInputStream(out.toByteArray());
    }

    public static ByteArrayInputStream getInvoice(Customer customer, List<Bill> bills, String dateFrom, String dateTo) throws IOException, DocumentException {
        Font font = new Font(Font.FontFamily.HELVETICA);
        Font headerFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);

        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(75);
        table.setWidths(new int[]{1, 3, 3});

        createCell(table, "ID", headerFont, Element.ALIGN_CENTER);
        createCell(table, "Description", headerFont, Element.ALIGN_CENTER);
        createCell(table, "Price(USD)", headerFont, Element.ALIGN_CENTER);

        createCell(table, "1", font, Element.ALIGN_CENTER);
        createCell(table, "Calls", font, Element.ALIGN_CENTER);
        createCell(table, String.valueOf(getPeriodTotal(bills, "voice")), font, Element.ALIGN_CENTER);

        createCell(table, "2", font, Element.ALIGN_CENTER);
        createCell(table, "SMS", font, Element.ALIGN_CENTER);
        createCell(table, String.valueOf(getPeriodTotal(bills, "sms")), font, Element.ALIGN_CENTER);

        createCell(table, "3", font, Element.ALIGN_CENTER);
        createCell(table, "Traffic", font, Element.ALIGN_CENTER);
        createCell(table, String.valueOf(getPeriodTotal(bills, "traffic")), font, Element.ALIGN_CENTER);

        createEmptyCell(table);
        createEmptyCell(table);

        PdfPCell cell = new PdfPCell(new Phrase("LINE TOTAL", font));
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);

        createEmptyCell(table);
        createCell(table, "SUBTOTAL", font, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 0);
        createCell(table, String.valueOf(getPeriodSubtotal(bills, "sms")
                + getPeriodSubtotal(bills, "traffic")
                + getPeriodSubtotal(bills, "voice")), font, Element.ALIGN_CENTER);

        createEmptyCell(table);
        createCell(table, "DISCOUNT", font, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 0);
        createCell(table, String.valueOf(getPeriodDiscount(bills, "sms")
                + getPeriodDiscount(bills, "traffic")
                + getPeriodDiscount(bills, "voice")), font, Element.ALIGN_CENTER);

        createEmptyCell(table);
        createCell(table, "TOTAL", font, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 0);
        createCell(table, String.valueOf(getPeriodTotal(bills, "sms")
                + getPeriodTotal(bills, "traffic")
                + getPeriodTotal(bills, "voice")), font, Element.ALIGN_CENTER);

        return createHeader(table, customer, "INVOICE", dateFrom, dateTo);
    }

    public static ByteArrayInputStream getFullPdfReport(Customer customer, List<Bill> bills, String dateFrom, String dateTo, String serviceType) throws IOException, DocumentException {

        Font font = new Font(Font.FontFamily.HELVETICA);
        Font headerFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);

        PdfPTable table = new PdfPTable(4);
        table.setWidthPercentage(75);
        table.setWidths(new int[]{3, 3, 2, 2});

        createCell(table, "Number", headerFont, Element.ALIGN_CENTER);
        createCell(table, "Total", headerFont, Element.ALIGN_CENTER);
        createCell(table, "Currency", headerFont, Element.ALIGN_CENTER);
        createCell(table, "Date", headerFont, Element.ALIGN_CENTER);

        for (Bill bill : bills) {
            createCell(table, bill.getCustomer().getServiceId().toString(), font, Element.ALIGN_CENTER);
            createCell(table, String.valueOf(round(bill.getTotal(), 2)), font, Element.ALIGN_CENTER);
            createCell(table, bill.getCurrencyCode(), font, Element.ALIGN_CENTER);
            String date = bill.getCreationDate().toString();
            createCell(table, date.substring(0, Math.min(date.length(), 10)), font, Element.ALIGN_CENTER);
        }

        return createHeader(table, customer, serviceType.toUpperCase(), dateFrom, dateTo);
    }
}